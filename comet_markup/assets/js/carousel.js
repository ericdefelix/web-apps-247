// BLAST! Carousel
// =========================================
//

// Initialize Variables
var carousel_id = "#" + $('.carousel').attr('id');
var li = carousel_id + ' .js_scroll-numbers li';
var li_active = carousel_id + ' .js_scroll-numbers li.active';

var $carousel_scroll = $(carousel_id).find('.js_carousel-scroll');
var $carousel_list = $(carousel_id).find('.js_carousel-list');
var $carousel_numbers = $(carousel_id).find('.js_scroll-numbers');

var step = $carousel_scroll.outerHeight() - 2;
var steplimit = $(carousel_id).find('.js_carousel-list').outerHeight() - step - 2; // 2 accommodates the 2px margin-bottom
var init_step = 0;
var tick = init_step; // Watch variable for offset updates

// Carousel Autoplay
var autoplay = true;
var carouseltimer;

function start_autoplay() {
    clearInterval(carouseltimer);
    carouseltimer = window.setInterval(function() {
       carousel_next_slide();
       console.log(tick);
    }, 6000);
}

// Prev Slide
function carousel_prev_slide(elem) {
    if (tick < step ) {
        // Go to last
        $(li).last().addClass('active');
        $(li).first().removeClass('active');

        tick = steplimit;
        $carousel_list.velocity( "scroll",
        {
            translateZ: 0, // Force Hardware Acceleration
            container: $carousel_scroll,
            duration: 450,
            easing: [ .99,.01,.33,1],
            offset: steplimit
        });

        start_autoplay();;
    }
    else {
        // Move previous
        $(li_active).prev().addClass('active');
        $(li_active).last().removeClass('active');

        tick = tick - step; // Update ticker
        $carousel_list.velocity( "scroll",  
        {
            translateZ: 0, // Force Hardware Acceleration
            container: $carousel_scroll,
            duration: 450,
            easing: [ .99,.01,.33,1],
            offset: tick
        });

        start_autoplay();
    }
}

// Next Slide
function carousel_next_slide(elem) {
    if (tick >= steplimit ) {

        // Go to first
        $(li).first().addClass('active');
        $(li).last().removeClass('active');

        tick = init_step;
        $carousel_list.velocity( "scroll",
        {
            translateZ: 0, // Force Hardware Acceleration
            container: $carousel_scroll,
            duration: 450,
            easing: [ .99,.01,.33,1],
            offset: init_step
        });

        start_autoplay();
    }
    else {
        // Move next
        $(li_active).next().addClass('active');
        $(li_active).first().removeClass('active');

        tick = tick + step; // Update ticker
        $carousel_list.velocity( "scroll",
        {
            translateZ: 0, // Force Hardware Acceleration
            container: $carousel_scroll,
            duration: 450,
            easing: [ .99,.01,.33,1],
            offset: tick
        });
    }
    start_autoplay();
}

// Count how many slides are to be rendered
function render_slidenum() {
    var list_len = Math.ceil( $carousel_list.children().length / 5 );

    for (var i = list_len - 1; i >= 0; i--) {
        $carousel_numbers.append('<li></li>');
    }
    
    $carousel_numbers.children().first().addClass('active');

    // Go to respective slide when slide number is clicked
    $(document).on('click', li, function() {
        var index = $(li).index(this) + 1;
        tick = (step * index) - step; // Update ticker

        $carousel_list.velocity("scroll",
        {
            translateZ: 0, // Force Hardware Acceleration
            container: $carousel_scroll,
            duration: 450,
            easing: [ .99,.01,.18,.98],
            offset: tick
        });

        // Tag who's active
        $(this).addClass('active');
        $(this).siblings('li').removeClass('active');
        start_autoplay();
    });
}

// Check if BLAST Button is clicked and Carousel is present
//var partialtimer;
//var carouselbuilt = false;
//$('#btnBlast').click(function() {
//    window.clearTimeout(partialtimer);         
//    partialtimer = window.setTimeout(function(){    
//        if ($('.carousel').length > 0 && carouselbuilt == false ) {
//            carouselbuilt = true; // Tell app carousel is already built
//            render_slidenum();
//            start_autoplay(); // Start autoplaying
//        }
//        else {
//            return false;
//        }
//    }
//    ,400);
//});

carouselbuilt = true; // Tell app carousel is already built
render_slidenum();
start_autoplay(); // Start autoplaying