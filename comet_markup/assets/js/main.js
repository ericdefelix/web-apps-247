// Initialize what needs to be hidden

var $post = $('.panel-body'); // The post's body

function initialize_feedback_comment() {
    $('.js_comment-edit-mode').hide();
}
// Initialize UI components

$(function () {

    // BS3 Components
    $('.help[data-toggle="popover"]').popover({ trigger: 'hover', 'placement': 'right' });


    $('body').tooltip({
        selector: '[data-toggle="tooltip"]'
    });

    $('[data-toggle="tooltip"]').tooltip({
        container: 'body'
    });

    // Ala-Facebook Scrolling
    $(".js_custom-scroll").mCustomScrollbar({
        autoHideScrollbar: true,
        scrollbarPosition: "outside",
        scrollInertia: 200,
        scrollButtons: { enable: true },
        advanced: {updateOnContentResize: true }
    });

    // Filter list
    $(document).off("click",".js_dropdown-select li a").on("click",".js_dropdown-select li a",function (event) {
        event.preventDefault();
        var item = $(this).closest('.js_dropdown-select').prev('button').find('.js_dropdown-select-value');
        $(item).text($(this).text());
        $(item).val($(this).text());
    });

    $('#frequencySelect input[name="sc_frequency"]').on('change', function () {
        if ($(this).val() == 'nofilter') {
            $('.js_employee-score-frequency').html('');
        }
        else {
            $('.js_employee-score-frequency').html($(this).data('value'));
        }

        if ($(this).data('value') != 'Weekly') {
            $('#ifWeeklySelected').addClass('hidden');
        }
        else {
            $('#ifWeeklySelected').removeClass('hidden');
        }
    });

    $('#generateReportType input[name="report_type"]').on('change', function () {
        if ($(this).data('value') == 'sc') {
            $('.js_sc').show().removeClass('hidden');
            $('.js_pmfb').addClass('hidden');
        }
        if ($(this).data('value') == 'pmfb') {
            $('.js_pmfb').show().removeClass('hidden');
            $('.js_sc').addClass('hidden');
        }
    });

})


// Strict Numbers Only on Input Field
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    var $num = $('#commitedValue');

    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }

    return true;
}

function isNumberAndDot(el, evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode != 45 && charCode != 8 && (charCode != 46) && (charCode < 48 || charCode > 57))
        return false;
    if (charCode == 46) {
        if ((el.value) && (el.value.indexOf('.') >= 0))
            return false;
        else
            return true;
    }
    return true;
}

// Commitment
// =========================================
// 

// Save Commitment
function save_commit(sender) {
    SaveCommitment(sender);
}

// Edit Commitment
function edit_commit(sender) {
    $(sender).removeClass('locked').addClass('unlocked');
}

// Cancel Commitment
function cancel_commit(sender) {
    $(sender).parents('.score-item-xs').find("#editCommit").removeClass('unlocked').addClass('locked');
    RevertCommit($(sender).parents(".score-item-xs").find("input"), $(sender).parents(".score-item-xs").find(".commit-edit-display").text());
}


// Feedback Controls
// =========================================
//

function view_comment_all(elem) {
    $(elem).closest('.panel-body').find('.js_comment').show();
    $(elem).hide();

    return false;
}

function write_comment(elem) {
    $(elem).closest('.panel-body').find('.js_reply').show();
    $(elem).closest('.panel-body').find('.js_comment-input').focus();
    $(elem).hide();
}

var thought_balloon = '.thought-balloon';

function edit_comment(elem) {
    $(elem).closest(thought_balloon).find('.js_comment-controls.display-comment').addClass('ishidden');
    $(elem).closest(thought_balloon).find('.js_comment-controls.btn-float').hide();
    $(elem).closest(thought_balloon).find('.js_comment-edit-mode').show();
    $(elem).closest(thought_balloon).find('.js_comment-edit-mode textarea').focus();
    $(elem).parents(".panel-body").find("form").find("textarea,button").attr("disabled","disabled");
}

function discard_comment(elem) {
    $(elem).closest(thought_balloon).find('.js_comment-controls.display-comment').removeClass('ishidden');
    $(elem).closest(thought_balloon).find('.js_comment-controls.btn-float').show();
    $(elem).closest(thought_balloon).find('.js_comment-edit-mode').hide();
    $(elem).parents(".js_comment-edit-mode").closest(thought_balloon).find('.js_comment-edit-mode textarea').val($(elem).parents(".js_comment-edit-mode").closest('.thought-balloon').find('.display-comment').text());
    $(elem).parents(".panel-body").find("form").find("textarea,button").removeAttr("disabled");
}

function delete_comment(elem) {
    if ($(elem).attr("delete-comment") == "blast") {
        DeleteBlastComment(elem);
    } 
    else {
        DeleteFeedbackComment(elem);
    }
}

function confirm_comment(elem) {

    $(elem).closest('.thought-balloon').find('.js_comment-controls').show();

    $(elem).closest('.thought-balloon').find('.js_comment-edit-mode').hide();

    var textarea = $(elem).closest('.thought-balloon').find("textarea");

    if ($(textarea).attr("update-comment") == 'feedback') {
        UpdateFeedbackComment($(textarea));
        $(elem).closest(thought_balloon).find('.js_comment-controls.display-comment').removeClass('ishidden');

    }
    else if ($(textarea).attr("update-comment") == 'blast') {
        UpdateBlastComment($(textarea));
        $(elem).closest(thought_balloon).find('.js_comment-controls.display-comment').removeClass('ishidden');

    } 

    $(elem).parents(".panel-body").find("form").find("textarea,button").removeAttr("disabled");
    
}

// Team Search
// =========================================
// 

// Toggle View of Panel
function toggle_team() {
    var $search = $('#searchParent');

    if ($search.hasClass('in')) {
        $('#searchParent').removeClass('in');
        $('#searchParent').velocity("fadeOut", { duration: 150, easing: [.99, .01, .33, 1] })
    }
    else {
        $('#searchParent').addClass('in');
        $('#searchParent').velocity("fadeIn", { duration: 150, easing: [.99, .01, .33, 1] })
        $('.js_search-input').focus();
    }

    resize_container_search();
}

function expand_directs(elem) {
    var spinner = '<i class="md md-settings md-spin md-2x"></i>';
    var caret = '<i class="md md-chevron-down md-2x"></i>';

    //$(elem).prop('disabled', true).css('opacity', '1');
    //$(elem).find('i').detach();
    //$(elem).append(spinner);

    $(elem).closest('.js_team-list-item').find('.js_team-sublist').toggleClass('js_init_hidden');
}

// Library Controls
function open_library_controls(elem) {
    $(elem).next('.js_library-controls').velocity("fadeIn", { duration: 150, easing: [.99, .01, .33, 1] });
}

function close_library_controls(elem) {
    $(elem).closest('.js_library-controls').velocity("fadeOut", { duration: 150, easing: [.99, .01, .33, 1] });
}

// Stretch container down to bottom
function resize_container_search() {
    var child = $('#teamListGroup');

    // Watch for parent's computed height. Relies on CSS' position fixed property
    var parent = $('#searchParent').outerHeight();
    var exclude = child.prev().outerHeight();

    child.css({ height: (parent - exclude) });
}

function resize_container_modal() {
    var child = $('.js_modal-container');
    var body = $(window).height();
    var totalHeight = body - 320;

    child.css({ height: totalHeight + 'px' });
    resize_container_modal_xs()
}

function resize_container_modal_xs() {
    var child = $('.js_modal-container-xs');
    var totalHeight = $('.js_modal-container').height() - 182;

    child.css({ height: totalHeight + 'px' }); 
}

$(document).on('click', '[data-target="#giveFeedback"], [data-target="#viewBlast"]', function() {
    resize_container_modal();
});

var resizeTimer;
$(window).resize(function () {
    clearTimeout(resizeTimer);
    resizeTimer = setTimeout(resize_container_search, 150);
    resizeTimer = setTimeout(resize_container_modal, 150);
    resizeTimer = setTimeout(resize_container_modal_xs, 150);
})

resize_container_search();


// Trends and Forecast
// function expand_panel(elem) {
//     $(elem).removeClass('closed');
// }

function expand_panel(elem) {
    $(elem).find('.js_panel-body').removeClass('js_init_hidden').addClass('flash');

    if ($(elem).hasClass('closed')) {
        $(elem).removeClass('closed');
    }
}


// Keyboard Shortcut Toggle Team
Mousetrap.bind(['alt+f'], function (e) {
    e.preventDefault();
    toggle_team();
});

// Keyboard Shortcut Discard Comment
Mousetrap.bind(['ctrl+enter'], function (e) {
    if ($(e.srcElement).is('textarea')) {
        if ($(e.srcElement).attr("update-comment") == 'feedback' || $(e.srcElement).attr("update-comment") == 'blast')
        {
            $(e.srcElement).closest('.thought-balloon').find('.js_confirm').click();
        } else if ($(e.srcElement).attr("update-comment") == 'newfeedback' || $(e.srcElement).attr("update-comment") == 'newblast') {
            $(e.srcElement).parents("form").find("button[type=submit]").click();
        }
        
    }
    else {
        return false;
    }
});

// Keyboard Shortcut Discard Comment
Mousetrap.bind(['esc'], function (e) {
    
    if ($(e.srcElement).is('textarea')) {
        $(e.srcElement).closest('.thought-balloon').find('.js_escape').click();
    }
    else {
        return false;
    }
});


// toastr.options = {
//     "closeButton": true,
//     "debug": false,
//     "newestOnTop": false,
//     "progressBar": true,
//     "positionClass": "toast-bottom-left",
//     "preventDuplicates": false,
//     "onclick": null,
//     "showDuration": "300",
//     "hideDuration": "1000",
//     "timeOut": "5000",
//     "extendedTimeOut": "1000",
//     "showEasing": "swing",
//     "hideEasing": "linear",
//     "showMethod": "fadeIn",
//     "hideMethod": "fadeOut"
// }
// IDP
var COMMENT_UI = {
    idp_field: '.js_inputFieldIDP',
    idp_info: '.js_infoIDP',
    parent_item: '.js_competencyItem',
    save_section: '.js_sectionSaveIDP',

    view_comments_action: '.js_btnViewComments',
    write_comment_action: '.js_btnWriteComment',
    edit_comment_action: '.js_btnEditComment',
    delete_comment_action: '.js_btnDeleteComment',
    confirm_delete_action: '.js_btnConfirmDelete',
    cancel_delete_action: '.js_btnCancelDelete',

    doc: $(document),

    bind: function() {
        this.doc.on('click', COMMENT_UI.view_comments_action, function() { COMMENT_UI.view_comments($(this)); });
        this.doc.on('click', COMMENT_UI.write_comment_action, function() { COMMENT_UI.write_comment($(this)); });
        this.doc.on('click', COMMENT_UI.edit_comment_action, function() { COMMENT_UI.edit_comment($(this),event); });
        this.doc.on('click', COMMENT_UI.delete_comment_action, function() { COMMENT_UI.delete_comment($(this),event); });
        this.doc.on('click', COMMENT_UI.confirm_delete_action, function() { COMMENT_UI.confirm_delete($(this),event); });
        this.doc.on('click', COMMENT_UI.cancel_delete_action, function() { COMMENT_UI.reset_comment_view($(this)); });

        // Reset view of the modal
        $('#careerIDPModal').on('hidden.bs.modal', COMMENT_UI.init_view );

        this.init_view();
    },
    init_view: function() {
        // Hide questions with more than 3
        COMMENT_UI.questions.each(function() {
            if ($(this).children().length > 3) { $(this).addClass('with-expand'); }
        });
    },
    view_comments: function(that) {
        that.closest('.panel-comment-thread').find('.js_commentItem').toggle();
        that.find('span:first').text() === 'View' ? that.find('span:first').text('Hide') : that.find('span:first').text('View');
    },
    write_comment: function(that) {
        that.hide();
        that.closest('.panel-comment-thread').siblings('.js_commentInput').show();
        that.closest('.panel-comment-thread').siblings('.js_commentInput').find('textarea').focus();
    },
    edit_comment: function(that,evt) {
        evt.preventDefault();
        that.closest('.js_thoughtBalloon').find('.js_commentViewMode').toggle();
        that.closest('.js_thoughtBalloon').find('.js_commentEditMode').toggle();
        if (that.closest('.js_thoughtBalloon').find('.js_commentEditMode:visible')) {
            that.closest('.js_thoughtBalloon').find('.js_commentEditMode textarea').focus();
        }
    },
    delete_comment: function(that,evt) {
        evt.preventDefault();
        that.closest('.js_commentItem').find('.panel-avatar,.panel-post-reply').velocity({opacity: 0.2});
        that.closest('.js_commentItem').find('.js_confirmDelete').show();
    },
    confirm_delete: function(that) {
        // Put this block after ajax call to delete the comment
        that.closest('.js_commentItem').velocity({opacity: 0}, { 
            display: 'none',
            duration: 300,
            complete: function(){ $(this).remove(); }
        });
        // Put this block after ajax call to delete the comment
    },
    reset_comment_view: function(that) {
        that.closest('.js_commentItem').find('.panel-avatar,.panel-post-reply').velocity({opacity: 1});
        that.closest('.js_commentItem').find('.js_confirmDelete').hide();
    },
    build: function() {
        this.bind();
    }
}

// PReview
var PREVIEW_UI = {
    list: $('#listIDP'),
    doc: $(document),
    item: [],
    count: 0,
    bind: function() {
        this.count = this.doc.find('#listIDP').children('li').length;
    },
    navigate_idp: function(direction) {
        // this.
        // .addClass('active-idp');
        // .removeClass('active-idp');
    },
    build: function() {
        this.bind();
    }
}


$(document).ready(function() {
    PREVIEW_UI.build();
    READ_MORE.build();
    STICKY_NAVBAR.build();
    SCROLL_TOP.build();
});