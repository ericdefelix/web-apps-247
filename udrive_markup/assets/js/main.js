// Carousel Navigation ===================================
var carousel = function () {

    var pos = 1,
        limit = 4,
        cur_pos = pos,
        picker = '.carousel-nav-picker li *[data-section-picker]',
        section = '.js_carousel-section';

    // Section Picker Button
    $('body').on('click', picker, function () {

        // Don't allow clicking on active and inactive
        if ($(this).data('state') === 'inactive' || $(this).data('state') === 'active') {
            return false;
        }
        else {
            //cur_pos = $(this).data('section-picker'); //Set active counter

            ////Tag as active
            //$(picker + '[data-section-picker="' + cur_pos + '"]').addClass('active').removeClass('done');
            //$(picker + '[data-section-picker="' + cur_pos + '"]').data('state', 'active');

            //// Remove active class from previous element
            //$(picker + '[data-section-picker!="' + cur_pos + '"]:not(.inactive)').removeClass('active').addClass('done');
            //$(picker + '[data-section-picker!="' + cur_pos + '"]:not(.inactive)').data('state', 'done');

            //// Show current section
            //$(section + '[data-section="' + cur_pos + '"]').addClass('active');

            //// Hide all inactive sections
            //$(section + '[data-section!="' + cur_pos + '"]').removeClass('active');
        }
    });

    this.PickerClick = function (pos) {
        //$(picker + '[data-section-picker!="' + pos + '"]').each(function () {
        //    $(this).addClass('inactive').removeClass('done').removeClass("active");
        //    $(this).data('state', 'inactive');
        //});

        //$(picker + '[data-section-picker="' + pos + '"]').addClass('active').removeClass('done').removeClass("inactive");
        //$(picker + '[data-section-picker="' + pos + '"]').data('state', 'active');

        $(picker).each(function () {
            if (pos == 4) {
                $(picker).addClass('inactive').removeClass('done').removeClass("active");
                $(picker).data('state', 'inactive');
            }

            if ($(this).data("section-picker") <= pos) {
                $(this).addClass('active').removeClass('done').removeClass("inactive");
                $(this).data('state', 'active');
            }
            else {
                $(this).addClass('inactive').removeClass('done').removeClass("active");
                $(this).data('state', 'inactive');
            }
        });

    }

}

// Toggle Sidebar View
var TOGGLE_SIDEBAR = {
    hidden: true,
    sidebar: $('#searchParent'),
    btn: $('#btnSearchParent'),
    scroll: $('#teamListGroup'),
    expand: $('.js_expandDirect'),
    sb_width: 0,
    expanded: false,

    init: function () {

        this.expand = $('.js_expandDirect');
        this.expanded = false;

        this.sb_width = this.sidebar.outerWidth();

        TOGGLE_SIDEBAR.btn.on('click', function () {
            TOGGLE_SIDEBAR.hidden == false ? TOGGLE_SIDEBAR.hide() : TOGGLE_SIDEBAR.show();
        });

        TOGGLE_SIDEBAR.expand.on('click', function () { TOGGLE_SIDEBAR.display_directs($(this)); });

        var tmr;
        $(window).on('resize', function () {
            tmr = setTimeout(TOGGLE_SIDEBAR.scrollbar(), 350);
            clearTimeout(tmr);
        });

        this.scrollbar();
    },

    scrollbar: function () {
        this.scroll.mCustomScrollbar({
            autoHideScrollbar: true,
            axis: 'y',
            contentTouchScroll: true,
            scrollbarPosition: 'inside',
            scrollInertia: 200,
            scrollButtons: { enable: false },
            theme: 'dark',
            advanced: { updateOnContentResize: true }
        });
        this.scroll.css({ height: TOGGLE_SIDEBAR.compute_height() });
    },

    compute_height: function () { return this.sidebar.height() - $('.team-input-group').outerHeight(); },

    hide: function () {
        TOGGLE_SIDEBAR.sidebar.velocity(
            { right: [-TOGGLE_SIDEBAR.sb_width, '0'] },
            {
                begin: function () {
                    TOGGLE_SIDEBAR.scroll.velocity({ opacity: 0 }, { duration: 300, easing: 'linear' });
                },
                duration: 500,
                easing: [.99, .01, .33, 1],
                complete: function () {
                    TOGGLE_SIDEBAR.sidebar.addClass('search-hidden');
                    TOGGLE_SIDEBAR.hidden = true;
                }
            }
        );
        TOGGLE_SIDEBAR.scroll.velocity({ opacity: [0, 1] }, { duration: 300, delay: 250, easing: 'linear' });
    },

    show: function () {
        TOGGLE_SIDEBAR.sidebar.velocity(
            { right: ['0', -TOGGLE_SIDEBAR.sb_width] },
            {
                begin: function () {
                    TOGGLE_SIDEBAR.sidebar.removeClass('search-hidden');
                    TOGGLE_SIDEBAR.hidden = false;
                },
                display: 'block',
                duration: 500,
                easing: [.99, .01, .33, 1]
            }
        );
        TOGGLE_SIDEBAR.scroll.velocity({ opacity: [1, 0] }, { duration: 300, delay: 400, easing: 'linear' });
    },

    display_directs: function (target) {

        if (this.expanded == false) {
            this.expanded = true;
            target.siblings('.js_teamSubList').show();
            target.siblings('.js_teamSubList').velocity({ translateY: ['0%', '-5%'], opacity: [1, 0] }, { duration: 300, easing: [.99, .01, .33, 1] });
            target.find('i').velocity({ rotateZ: '-180deg' }, { duration: 300, easing: [.99, .01, .33, 1] })
        }
        else {
            this.expanded = false;
            target.siblings('.js_teamSubList').velocity({ translateY: '-5%', opacity: 0 }, {
                duration: 300, easing: [.99, .01, .33, 1], complete: function () {
                    target.siblings('.js_teamSubList').hide();
                }
            });
            target.find('i').velocity('reverse');
        }
    },

    build: function () {
        this.init();
    }
}


//set first tab on preferred roles as active
// var SetActiveFirstTab = function () {
//     $('.select-role ul li:first a').click();
// }

// Career IDP
var CAREER_IDP_UI = {
    idp_field: '.js_inputFieldIDP',
    idp_info: '.js_infoIDP',
    parent_item: '.js_competencyItem',
    save_section: '.js_sectionSaveIDP',

    add_action: '.js_btnAddIDP',
    save_action: '.js_btnSaveIDP',
    discard_action: '.js_btnDiscardIDP',

    confirm_comment_action: '.js_btnConfirmComment',
    view_comments_action: '.js_btnViewComments',
    write_comment_action: '.js_btnWriteComment',
    edit_comment_action: '.js_btnEditComment',
    delete_comment_action: '.js_btnDeleteComment',
    confirm_delete_action: '.js_btnConfirmDelete',
    cancel_delete_action: '.js_btnCancelDelete',

    questions: $('.list-competencies-questions'),
    plan: $('.js_staticActionPlan'),
    doc: $(document),

    footer: $('#careerIDPModalFooter'),

    bind: function () {
        this.doc.on('click', CAREER_IDP_UI.add_action, function () { CAREER_IDP_UI.add($(this)); });
        this.doc.on('click', CAREER_IDP_UI.save_action, function () { CAREER_IDP_UI.save($(this)); });
        this.doc.on('click', CAREER_IDP_UI.discard_action, function () { CAREER_IDP_UI.hide($(this)); });
        this.doc.on('click', CAREER_IDP_UI.view_comments_action, function () { CAREER_IDP_UI.view_comments($(this)); });
        this.doc.on('click', CAREER_IDP_UI.confirm_comment_action, function () { CAREER_IDP_UI.confirm_comment($(this), event); });
        this.doc.on('click', CAREER_IDP_UI.write_comment_action, function () { CAREER_IDP_UI.write_comment($(this)); });
        this.doc.on('click', CAREER_IDP_UI.edit_comment_action, function () { CAREER_IDP_UI.edit_comment($(this), event); });
        this.doc.on('click', CAREER_IDP_UI.delete_comment_action, function () { CAREER_IDP_UI.delete_comment($(this), event); });
        this.doc.on('click', CAREER_IDP_UI.confirm_delete_action, function () { CAREER_IDP_UI.confirm_delete($(this), event); });
        this.doc.on('click', CAREER_IDP_UI.cancel_delete_action, function () { CAREER_IDP_UI.reset_comment_view($(this)); });

        this.doc.on('click', '#btnEditTimeline', function () { CAREER_IDP_UI.edit_timeline($(this)); });
        this.doc.on('click', '#btnSaveTimeline', function () { CAREER_IDP_UI.save_timeline($(this)); });
        this.doc.on('click', '#btnDiscardTimeline', function () { CAREER_IDP_UI.view_timeline($(this)); });

        this.doc.on('click', '#btnDiscardTimeline', function () { CAREER_IDP_UI.view_timeline($(this)); });

        this.doc.on('click', '.list-competencies-questions', function () {
            $(this).removeClass('with-expand');
            $(this).children('li').removeClass('ng-hide');
        });

        // Reset view of the modal
        this.doc.on('hide.bs.modal', '#careerIDPModal', CAREER_IDP_UI.init_view);
        // this.doc.on('shown.bs.modal', '#careerIDPModal', CAREER_IDP_UI.check_footer );
        // this.init_view();
    },
    fixed_footer: function () {
        var h1 = this.doc.find('#careerIDPModal .modal-dialog').height() - this.doc.find(this.footer).height(),
            h2 = this.doc.find('#careerIDPModal').height();

        if (h1 > h2) { return true; }
        else { return false; }
    },
    check_footer: function () {
        if (CAREER_IDP_UI.fixed_footer() === true) {
            $('#careerIDPModalBody').find('.panel-body').css('paddingBottom', '60px');
            this.doc.find('#careerIDPModalFooter').detach().appendTo('.modal-footer-fixed').hide();
            this.doc.find('#careerIDPModalFooter').velocity({ opacity: [1, 0] }, { display: 'block', easing: [0, 0.96, 0.86, 0.99], duration: 150 });
        }
    },
    init_view: function () {
        // Hide questions with more than 3
        $(document).find('.list-competencies-questions').each(function () {
            if ($(this).children().length > 3) { $(this).addClass('with-expand'); }
            $(this).find('li').slice(3).hide();
        });

        CAREER_IDP_UI.check_footer();

        $('#careerIDPModalBody').find('.panel-body').removeAttr('style');

        if (CAREER_IDP_UI.fixed_footer() === false) {
            $(document).find(this.footer).show();
        }
        else {
            $(document).find(this.footer).removeAttr('style').hide();
        }
    },
    add: function (that) {
        that.hide();
        that.closest(this.parent_item).find(this.idp_field).removeClass('hidden');
        that.closest(this.parent_item).find(this.save_section).removeClass('hidden');
        if (this.plan.length) { this.plan.hide(); }
    },
    save: function (that) {
        // some ajax saving and if done execute below
        this.hide(that);
        if (this.plan.length) { this.plan.show(); }
    },
    edit_timeline: function (that) {
        that.closest('.js_timeline').find('.js_editTimeline').show();
        that.closest('.js_timeline').find('.js_viewTimeline').hide();
    },
    view_timeline: function (that) {
        that.closest('.js_timeline').find('#btnEditTimeline').show();
        that.closest('.js_timeline').find('.js_editTimeline').hide();
        that.closest('.js_timeline').find('.js_viewTimeline').show();
    },
    save_timeline: function (that) {
        // put ajax here, if success execute below
        that.closest('.js_timeline').find('.js_editTimeline').hide();
        that.closest('.js_timeline').find('.js_viewTimeline').show();
    },
    hide: function (that) {
        that.closest(this.parent_item).find(this.save_section).addClass('hidden');
        that.closest(this.parent_item).find(this.idp_field).addClass('hidden');
        that.closest(this.parent_item).find(this.idp_info).show();
        that.closest(this.parent_item).find('.js_btnAddIDP').show();
        if (this.plan.length) { this.plan.show(); }
    },
    confirm_comment: function (that, evt) {
        var textarea = $(that).closest('.thought-balloon').find("textarea");
        UpdateCareerComment($(textarea));
        evt.preventDefault();
        that.closest('.js_thoughtBalloon').find('.js_commentViewMode').toggle();
        that.closest('.js_thoughtBalloon').find('.js_commentEditMode').toggle();
        if (that.closest('.js_thoughtBalloon').find('.js_commentEditMode:visible')) {
            that.closest('.js_thoughtBalloon').find('.js_commentEditMode textarea').focus();
        }
    },
    view_comments: function (that) {
        that.closest('.panel-comment-thread').toggleClass('toggled');
        that.find('span:first').text() === 'View' ? that.find('span:first').text('Hide') : that.find('span:first').text('View');
    },
    write_comment: function (that) {
        that.hide();
        that.closest('.panel-comment-thread').siblings('.js_commentInput').show();
        that.closest('.panel-comment-thread').siblings('.js_commentInput').find('textarea').focus();
    },
    edit_comment: function (that, evt) {
        evt.preventDefault();
        that.closest('.js_thoughtBalloon').find('.js_commentViewMode').toggle();
        that.closest('.js_thoughtBalloon').find('.js_commentEditMode').toggle();
        if (that.closest('.js_thoughtBalloon').find('.js_commentEditMode:visible')) {
            that.closest('.js_thoughtBalloon').find('.js_commentEditMode textarea').focus();
        }
    },
    delete_comment: function (that, evt) {
        evt.preventDefault();
        // that.closest('.js_commentItem').find('.panel-avatar,.panel-post-reply').velocity({opacity: 0.2});
        that.closest('.js_commentItem').find('.js_confirmDelete').show();
    },
    confirm_delete: function (that) {
        // Put this block after ajax call to delete the comment
        $(that).parents('.js_confirmDelete').velocity({ opacity: 0 }, {
            display: 'none',
            duration: 300,
            complete: function () {
                $(this).remove();
            }
        });
        // Put this block after ajax call to delete the comment
    },
    reset_comment_view: function (that) {
        that.closest('.js_commentItem').find('.panel-avatar,.panel-post-reply').velocity({ opacity: 1 });
        that.closest('.js_commentItem').find('.js_confirmDelete').hide();
    },
    build: function () {
        this.bind();
    }
}

//TUTORIAL
var TUTORIAL = {
    doc: $(document),
    parent: $('#tutorialPanels'),
    gallery: '.tab-pane .tutorial-gallery',
    active_gallery: '.tab-pane.active .tutorial-gallery',
    pointer: 1,

    bind: function () {
        this.doc.on('click', '#prevImageBtn', function () { TUTORIAL.change_image('prev'); });
        this.doc.on('click', '#nextImageBtn', function () { TUTORIAL.change_image('next'); });
        this.doc.on('click', 'ul.tutorial-slide > li a', function () { TUTORIAL.reset_view(); }); //replaced ID selector, added class on markup
        this.doc.on('hidden.bs.modal', '#modalHelp', TUTORIAL.init_view);

        this.doc.on('click', '[data-toggle="subtab"]', function () {
            var pane = $(this).data('target');
            $(this).closest('li').addClass('active');
            $(this).closest('li').siblings().removeClass('active');
            $(this).closest('.pnav').removeClass('active');

            $(pane).addClass('active');
            $(pane).siblings().removeClass('active');

            TUTORIAL.reset_view(); // inserted reset_view
            TUTORIAL.get_total();
        });

        $('.pnav').on('click', function () {
            $(this).addClass('subnav-open');
            $(this).siblings().removeClass('subnav-open');
            $(this).find('.sub-nav').children('li').removeClass('active');
            TUTORIAL.get_total();
        });


        TUTORIAL.init_view();
        TUTORIAL.get_total();
    },
    change_image: function (direction) {
        var totalChild = TUTORIAL.parent.find(TUTORIAL.active_gallery).children('.slide').length;

        if (direction == 'next') {
            if (this.pointer == totalChild) { this.pointer = 1; }
            else { this.pointer++; }
        }

        else if (direction == 'prev') {
            if (this.pointer == 1) { this.pointer = totalChild; }
            else { this.pointer--; }
        }

        $('#galleryPointer').text(TUTORIAL.pointer);

        $(TUTORIAL.active_gallery).children('.slide').eq(TUTORIAL.pointer - 1).show();
        $(TUTORIAL.active_gallery).children('.slide').eq(TUTORIAL.pointer - 1).siblings().hide(); // replaced (TUTORIAL.pointer - 2).hide();
    },
    get_total: function () {
        var count = $(TUTORIAL.active_gallery).children('.slide').length;
        if (count !== 0) {
            $('#galleryCount').text(count);
            $('#galleryPointer').text(TUTORIAL.pointer);
            $('#prevImageBtn,#nextImageBtn').removeAttr('disabled');
        }
        else {
            $('#galleryCount,#galleryPointer').text(count);
            $('#prevImageBtn,#nextImageBtn').attr('disabled', 'true');
        }
        return count;
    },
    init_view: function () {
        this.pointer = 1;
        $(TUTORIAL.active_gallery).children('.slide').eq(0).show();
        $(TUTORIAL.active_gallery).children('.slide').eq(0).siblings().hide();
    },
    reset_view: function () {
        this.init_view();
        this.get_total();
        this.pointer = 1;
    },
    build: function () {
        this.bind();
    }
}

var UI_HELPERS = {
    doc: $(document),
    bind: function () {
        this.doc.on('click', '*[data-toggle="section"]', function () {
            $(this).addClass('active');
            $(this).siblings('*[data-toggle="section"]').removeClass('active');
        });

        this.doc.find('*[data-toggle="tooltip"]').tooltip({ animation: false });
        this.doc.find('*[data-toggle="popover"]').popover({ animation: false });
        this.doc.on('click', '*[data-dismiss="popover"]', function () {
            $(this).closest('.popover').popover('hide');
        })
        .on('click', '.popover-init', function () { // for 9box popover instruction
            $(this).popover('show');
        });;
        // Invoke the Custom rating: base element select
        if ($('.js_rating').length) $('.js_rating').barrating({ theme: 'bars-movie' });;

        // On scroll down, convert to sticky
        $(window).scroll(function () {
            try {
                var scrollTop = $(window).scrollTop();
                docWidth = $(document).width(),
                profile = $('.profile-info'),
                btns = $('.js_navbar-controls'),
                _threshold = profile.height(),
                _top = profile.offset().top;

                if (docWidth > 991 && scrollTop > _threshold) {
                    btns.addClass('navbar-sticky').css('top', _top);
                    profile.css('marginBottom', '60px');
                }
                else if (docWidth > 991 && scrollTop < _threshold) {
                    btns.removeClass('navbar-sticky').css('top', 'auto');
                    profile.css('marginBottom', '0');
                }
            } catch (e) {

            }

        });

        this.doc.on('click', '#btnToggleHelp', function () {
            $('#roleSelectionHelp').toggle();
            $(this).toggleClass('toggled');
            $(this).find('span').text() === 'Need' ? $(this).find('span').text('Hide') : $(this).find('span').text('Need');
        });

        this.doc.on('click', '#btnToggleSelectedRoles', function () {
            $('#selectedRoles').toggle().toggleClass('selected-roles-fixed');
            $(this).text() === 'Edit Selected Roles' ? $(this).text('Exit Selected Roles') : $(this).text('Edit Selected Roles');
        });

        this.doc.find('.js_limit-text').each(function () {
            LimitText($(this).attr("maxlength"), $(this));
        });
    },
    build: function () {
        this.bind();
    }
}

// FOR 9-BOX CUSTOM DROPDOWNS
var NINEBOX_UI = {
    obj: [],
    doc: $(document),
    filter: $('#directReportsFilter'),
    list: $('.tab-pane.active .quadrant-list .quadrant-list-item'),
    quadrant: $('.nine-box-quadrant'),

    bind: function () {
        this.doc.on('click', '[data-toggle="dropdown-custom"]', function () { NINEBOX_UI.toggle_menu($(this)); });
        this.doc.on('click', '.close-custom-dropdown', function () { NINEBOX_UI.close_opened_menu($(this)); });

        this.doc.on('click', 'body', function (event) {
            var dd = $(event.target).closest('.dropdown'),
                close_dropdown = $('.close-custom-dropdown');

            if (dd.length == 0 && close_dropdown.length == 1) {
                $('.dropdown').removeClass('open');
                close_dropdown.remove();
                NINEBOX_UI.obj = [];
            }
        });

        this.doc.on('keyup', '#directReportsFilter', NINEBOX_UI.toggle_item_view);

        $(window).on('resize', function () {
            NINEBOX_UI.resize_cells();
        });

        this.doc.find('header.navbar ul li a').on('click', NINEBOX_UI.is_container_fluid);
    },

    is_container_fluid: function () {
        var box = $(document).find('#nineboxresult');
        console.log(box.length);
        if (box.length) {
            this.doc.find('#mainContainer').addClass('container-fluid').removeClass('container');
        }
        else {
            this.doc.find('#mainContainer').addClass('container').removeClass('container-fluid');
        }
    },

    toggle_menu: function (elem) {
        var dd = elem.parent('.dropdown');

        dd.addClass('open');
        dd.prepend('<span class="close-custom-dropdown"></span>');

        NINEBOX_UI.obj.push(elem);

        if (NINEBOX_UI.obj.length == 2) {
            NINEBOX_UI.obj[0].parent('.dropdown').removeClass('open');
            NINEBOX_UI.obj[0].prev().remove();
        }
        if (NINEBOX_UI.obj.length > 2) {
            NINEBOX_UI.obj.shift();
            NINEBOX_UI.obj[0].parent('.dropdown').removeClass('open');
            NINEBOX_UI.obj[0].prev().remove();
        }
    },

    toggle_item_view: function () {
        this.doc.find('.tab-pane.active .quadrant-list .quadrant-list-item').each(function () {
            if ($(this).data('original-title').search(new RegExp(NINEBOX_UI.filter.val(), "i")) < 0) {
                $(this).closest('.quadrant-list-item').hide();
            }
            else {
                $(this).closest('.quadrant-list-item').show();
            }
        });

        if (NINEBOX_UI.list.find(':visible').length === 0) { NINEBOX_UI.list.parent().addClass('no-direct-reports'); }
        else { NINEBOX_UI.list.parent().removeClass('no-direct-reports'); }
    },

    close_opened_menu: function (elem) {
        NINEBOX_UI.obj = [];
        elem.parent('.dropdown').removeClass('open');
        elem.remove();
    },

    toggle_view: function (elem) {
        var text_fit = '<i class="md md-fullscreen md-lg"></i> Fit To Page',
            text_revert = '<i class="md md-fullscreen-exit md-lg"></i> Restore Page',
            timeout;

        $(elem).html() == text_fit ? $(elem).html(text_revert) : $(elem).html(text_fit);
        $('body').toggleClass('h_expanded-ninebox');

        timeout = setTimeout(function () {
            NINEBOX_UI.resize_cells();
            clearTimeout(timeout);
        }, 200);
    },
    resize_cells: function () {
        var viewport_height = $(window).outerHeight();
        height = (viewport_height / 3) - 60 - 35;

        var quadrant = this.doc.find('.nine-box-quadrant');

        quadrant[0].hasAttribute("style") ? quadrant.removeAttr('style') : quadrant.css('height', height + 'px');;
    },

    build: function () {
        this.bind();
    }
}

$(document).ready(function () {
    carousel();
    NINEBOX_UI.build();
    TUTORIAL.build();
    TOGGLE_SIDEBAR.build();
    // CAREER_IDP_UI.build();
    UI_HELPERS.build();
});
