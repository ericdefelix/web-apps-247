//--------------------------------------------------------------------------------
// Build Environment
//--------------------------------------------------------------------------------

// Generate mountains
var sp = {
	stall: ['stall-one','stall-two','stall-three','stall-four'],
	bush: ['bush-one','bush-two','bush-three','bush-four','bush-five'],
	flowerpot: ['flowerpot-one','flowerpot-two','flowerpot-three','flowerpot-four','flowerpot-five'],
	flower: ['flower-one','flower-two','flower-three','flower-four'],
	tree: ['tree-one','tree-two','tree-three','tree-four','tree-five','tree-six','tree-seven','tree-eight','tree-nine','tree-ten','tree-eleven'],
	bench: ['bench-one','bench-two','bench-three','bench-four'],
	bin: ['bin-one','bin-two','bin-three'],
	flag: ['flag-one','flag-two','flag-three','flag-four'],
}
var ENVIRONMENT = {
	space: $('.building-surroundings'), // 18
	get_elem: function(className,left,top,zindex) { elem = '<i class="'+ className +'" style="left:'+ left +'px; top: '+ top +'px; z-index:' + zindex +';"></i>'; return elem; },
	build: function() {
		this.space.eq(1).append(
			this.get_elem(sp.tree		[0],100,62,1) + 
			this.get_elem(sp.bush		[4],-21,202,3) +
			this.get_elem(sp.bush		[0],408,186.5,1) +
			this.get_elem(sp.tree		[10],443,126,1) +
			this.get_elem(sp.tree		[0],225,62,1) +
			this.get_elem(sp.bench		[2],146,184.5,1) +
			this.get_elem(sp.tree		[3],329,95.5,1) +
			this.get_elem(sp.flowerpot	[3],434,183,1) +
			this.get_elem(sp.bush		[3],283,197.5,1) +
			this.get_elem(sp.bush		[3],31,191,1) +
			this.get_elem(sp.flower		[3],-19,192.5,3)
		);
		this.space.eq(2).append(
			this.get_elem(sp.tree		[1],5,90.5,1) + 
			this.get_elem(sp.tree		[5],54,155,1) + 
			this.get_elem(sp.tree		[3],132,96.5,1) + 
			this.get_elem(sp.tree		[1],340,83.5,1) + 
			this.get_elem(sp.tree		[10],417,128,1) + 
			this.get_elem(sp.bush		[4],169,201,1) + 
			this.get_elem(sp.bush		[2],-15,205,1) + 
			this.get_elem(sp.bush		[3],275,200.5,1) + 
			this.get_elem(sp.flowerpot	[2],404,184,1) + 
			this.get_elem(sp.flowerpot	[1],379,186.5,1) + 
			this.get_elem(sp.flower		[5],49,188,1) + 
			this.get_elem(sp.flower		[2],64,190,1) + 
			this.get_elem(sp.flower		[3],75,189.5,1) + 
			this.get_elem(sp.bin		[2],106,184,1)
		);
		this.space.eq(3).append(
			this.get_elem(sp.bench		[0],76,189,1) + 
			this.get_elem(sp.bench		[1],325,192.5,1) + 
			this.get_elem(sp.bush		[1],-18,199,1) + 
			this.get_elem(sp.bush		[4],433,200,1) + 
			this.get_elem(sp.tree		[3],391,104.5,1) + 
			this.get_elem(sp.tree		[9],15,119.5,1) + 
			this.get_elem(sp.tree		[7],147,120,1) + 
			this.get_elem(sp.tree		[2],195,108.5,1) + 
			this.get_elem(sp.tree		[2],249,118.5,1)
		);
		this.space.eq(4).append(
			this.get_elem(sp.flag		[3],256,45,1) + 
			this.get_elem(sp.bush		[4],155,205,2) + 
			this.get_elem(sp.bush		[2],399,189.5,1) + 
			this.get_elem(sp.bush		[3],369,198.5,1) + 
			this.get_elem(sp.tree		[9],114,113.5,1) + 
			this.get_elem(sp.tree		[2],185,106,1) + 
			this.get_elem(sp.tree		[0],70,70,1) + 
			this.get_elem(sp.bush		[2],11,199.5,1) + 
			this.get_elem(sp.flower		[2],430,190,1) + 
			this.get_elem(sp.flower		[0],225,194,1) + 
			this.get_elem(sp.flower		[3],461,191.5,1) + 
			this.get_elem(sp.tree		[9],455,113.5,1) + 
			this.get_elem(sp.tree		[5],-2,160.5,1)
		);
		this.space.eq(5).append(
			this.get_elem(sp.tree		[3],-5,98.5,1) + 
			this.get_elem(sp.tree		[1],118,85.5,1) + 
			this.get_elem(sp.tree		[3],184,96.5,1) + 
			this.get_elem(sp.tree		[9],58,125.5,1) + 
			this.get_elem(sp.tree		[9],311,104.5,1) + 
			this.get_elem(sp.tree		[3],386,98.5,1) + 
			this.get_elem(sp.tree		[7],236,107,1)
		);
		this.space.eq(6).append(
			this.get_elem(sp.stall		[1],31,125.5,1) + 
			this.get_elem(sp.bush		[4],-6,210,1) + 
			this.get_elem(sp.stall		[2],222,128.5,1) + 
			this.get_elem(sp.tree		[10],428,122,1) + 
			this.get_elem(sp.bin		[1],411,167,1) + 
			this.get_elem(sp.bush		[2],339,200.5,1) + 
			this.get_elem(sp.flower		[2],371,190,1) + 
			this.get_elem(sp.flower		[2],359,193,1) + 
			this.get_elem(sp.flower		[1],388,194,1) + 
			this.get_elem(sp.bush		[3],467,196.5,1)
		);
		this.space.eq(7).append(
			this.get_elem(sp.flag		[1],253,43,1) +
			this.get_elem(sp.bush		[2],159,201,1) +
			this.get_elem(sp.bush		[4],282,192,1) +
			this.get_elem(sp.bush		[3],335,201,1) +
			this.get_elem(sp.bush		[5],262,160,1) +
			this.get_elem(sp.tree		[10],0,125,1) +
			this.get_elem(sp.tree		[1],56,90,1) +
			this.get_elem(sp.tree		[10],474,123,1) +
			this.get_elem(sp.tree		[4],386,150,1) +
			this.get_elem(sp.tree		[1],430,90,1) +
			this.get_elem(sp.bush		[5],335,201,1) + 
			this.get_elem(sp.bush		[1],105,204,1)
		);
		this.space.eq(8).append(
			this.get_elem(sp.bin		[1],4,166,1) +
			this.get_elem(sp.tree		[2],416,114.5,1) +
			this.get_elem(sp.tree		[3],29,104.5,1) +
			this.get_elem(sp.tree		[2],121,115.5,1) +
			this.get_elem(sp.tree		[3],262,96.5,1) +
			this.get_elem(sp.tree		[9],333,121.5,1) +
			this.get_elem(sp.tree		[9],197,120.5,1) +
			this.get_elem(sp.flowerpot	[4],460,195.5,1) +
			this.get_elem(sp.flowerpot	[2],-24,185.5,1) +
			this.get_elem(sp.flowerpot	[2],45,190.5,1) +
			this.get_elem(sp.flowerpot	[0],-39,183.5,1) +
			this.get_elem(sp.bush		[3],84,202.5,1) +
			this.get_elem(sp.bush		[3],178,204.5,1) +
			this.get_elem(sp.stall		[3],300,177,1)
		);
		this.space.eq(9).append(
			this.get_elem(sp.flag		[2],253,43,1) +
			this.get_elem(sp.bush		[2],159,201,1) +
			this.get_elem(sp.bush		[4],282,192,1) +
			this.get_elem(sp.bush		[3],335,201,1) +
			this.get_elem(sp.bush		[5],262,160,1) +
			this.get_elem(sp.tree		[10],0,125,1) +
			this.get_elem(sp.tree		[1],56,90,1) +
			this.get_elem(sp.tree		[10],474,123,1) +
			this.get_elem(sp.tree		[4],386,150,1) +
			this.get_elem(sp.tree		[8],430,130,1) +
			this.get_elem(sp.tree		[5],-32,159,1) +
			this.get_elem(sp.bush		[5],335,201,1) + 
			this.get_elem(sp.bush		[1],105,204,1)
		);
		this.space.eq(10).append(
			this.get_elem(sp.stall		[2],374,134.5,1) +
			this.get_elem(sp.tree		[2],0,106.5,1) +
			this.get_elem(sp.tree		[2],471,109.5,1) +
			this.get_elem(sp.tree		[2],311,108.5,1) +
			this.get_elem(sp.tree		[2],62,108.5,1) +
			this.get_elem(sp.tree		[5],-2,160.5,1) +
			this.get_elem(sp.tree		[9],265,113.5,1) +
			this.get_elem(sp.tree		[7],97,114,1) +
			this.get_elem(sp.bush		[2],136,190.5,1) +
			this.get_elem(sp.bush		[4],228,195,1) +
			this.get_elem(sp.bench		[3],122,183.5,1) +
			this.get_elem(sp.bench		[3],241,184.5,1) +
			this.get_elem(sp.flower		[0],81,192,1) +
			this.get_elem(sp.flower		[2],60,189,1)
		);
		this.space.eq(11).append(
			this.get_elem(sp.tree		[2],14,110.5,1) +
			this.get_elem(sp.tree		[2],464,107.5,1) +
			this.get_elem(sp.tree		[3],79,97.5,1) +
			this.get_elem(sp.tree		[3],395,96.5,1) +
			this.get_elem(sp.tree		[2],270,108.5,1) +
			this.get_elem(sp.tree		[9],343,113.5,1) +
			this.get_elem(sp.tree		[7],51,119,1) +
			this.get_elem(sp.tree		[8],230,126.5,1) +
			this.get_elem(sp.bush		[3],146,189.5,1) +
			this.get_elem(sp.bush		[2],-8,194.5,1) +
			this.get_elem(sp.bush		[3],410,202.5,1) +
			this.get_elem(sp.bin		[2],19,183,1) +
			this.get_elem(sp.stall		[1],71,126.5,1) +
			this.get_elem(sp.stall		[0],253,125.5,1)
		);
		this.space.eq(12).append(
			this.get_elem(sp.tree		[9],-5,128.5,1) + 
			this.get_elem(sp.tree		[9],118,128.5,1) + 
			this.get_elem(sp.tree		[3],184,96.5,1) + 
			this.get_elem(sp.tree		[1],58,124.5,1) + 
			this.get_elem(sp.tree		[7],381,124,2) + 
			this.get_elem(sp.tree		[3],386,98.5,1) + 
			this.get_elem(sp.bench		[2],276,187,2)
		);
		this.space.eq(13).append(
			this.get_elem(sp.bin		[1],0,168,2) +
			this.get_elem(sp.tree		[9],437,114.5,2) +
			this.get_elem(sp.tree		[3],332,95.5,2) +
			this.get_elem(sp.tree		[9],204,111.5,2) +
			this.get_elem(sp.tree		[3],75,104.5,2) +
			this.get_elem(sp.bush		[4],139,192,2) +
			this.get_elem(sp.bush		[2],42,202.5,2) +
			this.get_elem(sp.bush		[3],404,198.5,2) +
			this.get_elem(sp.bush		[1],244,197,2) +
			this.get_elem(sp.flower		[2],206,191,2) +
			this.get_elem(sp.flower		[0],172,192,2) +
			this.get_elem(sp.flower		[0],391,194,2) +
			this.get_elem(sp.flower		[2],381,193,2) +
			this.get_elem(sp.flower		[3],155,192.5,2) +
			this.get_elem(sp.flowerpot	[4],498,193.5,2) +
			this.get_elem(sp.bush		[1],310,203.5,2)
		);
		this.space.eq(14).append(
			this.get_elem(sp.tree		[9],-5,113.5,1) + 
			this.get_elem(sp.tree		[9],118,112.5,1) + 
			this.get_elem(sp.tree		[3],184,108.5,1) + 
			this.get_elem(sp.tree		[1],58,92.5,1) + 
			this.get_elem(sp.tree		[7],381,124,2) + 
			this.get_elem(sp.tree		[3],386,98.5,1) + 
			this.get_elem(sp.bench		[1],284,187,2)
		);
		this.space.eq(15).append(
			this.get_elem(sp.flag		[0],253,43,1) +
			this.get_elem(sp.bush		[2],159,201,1) +
			this.get_elem(sp.bush		[4],282,192,1) +
			this.get_elem(sp.bush		[3],335,201,1) +
			this.get_elem(sp.bush		[5],262,160,1) +
			this.get_elem(sp.tree		[9],0,125,1) +
			this.get_elem(sp.tree		[1],56,90,1) +
			this.get_elem(sp.tree		[10],474,123,1) +
			this.get_elem(sp.tree		[4],386,150,1) +
			this.get_elem(sp.tree		[1],430,90,1) +
			this.get_elem(sp.bush		[5],335,201,1) + 
			this.get_elem(sp.bush		[1],105,204,1)
		);
		this.space.eq(16).append(
			this.get_elem(sp.bench		[0],76,189,1) + 
			this.get_elem(sp.bench		[1],325,192.5,1) + 
			this.get_elem(sp.bush		[1],-18,199,1) + 
			this.get_elem(sp.bush		[4],433,200,1) + 
			this.get_elem(sp.tree		[3],391,104.5,1) + 
			this.get_elem(sp.tree		[9],15,119.5,1) + 
			this.get_elem(sp.tree		[7],147,120,1) + 
			this.get_elem(sp.tree		[2],195,108.5,1) + 
			this.get_elem(sp.tree		[2],249,118.5,1)
		);
		this.space.eq(17).append(
			this.get_elem(sp.tree 		[2],5,106.5,1) +
			this.get_elem(sp.tree 		[2],310,106.5,1) +
			this.get_elem(sp.tree 		[3],97,95.5,1) +
			this.get_elem(sp.tree 		[3],418,96.5,1) +
			this.get_elem(sp.tree 		[9],38,122.5,1) +
			this.get_elem(sp.tree 		[9],353,123.5,1) +
			this.get_elem(sp.tree 		[7],163,123,1) +
			this.get_elem(sp.tree 		[1],240,79.5,1) +
			this.get_elem(sp.tree 		[8],291,133.5,1) +
			this.get_elem(sp.flowerpot 	[3],496,183,1) +
			this.get_elem(sp.flowerpot 	[2],427,185.5,1) +
			this.get_elem(sp.flowerpot 	[2],99,187.5,1) +
			this.get_elem(sp.flowerpot 	[4],339,191.5,1) +
			this.get_elem(sp.flowerpot 	[3],128,184,1) +
			this.get_elem(sp.bush 		[3],214,196.5,1) +
			this.get_elem(sp.bush 		[1],-13,200,1)
		);
	}
}
