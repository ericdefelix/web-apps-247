var table_responsive_stack = {
    tbl: $('.table-stack-responsive'),
    init: function () {
        var tbl = table_responsive_stack.tbl;
        if (tbl.length) {
            var tbl_parent = tbl.parent(),
                tbl_header = tbl.children('thead').find('th'),
                tbl_row = tbl.children('tbody').children('tr'),
                tbl_cell = tbl_row.children('td');

            // detaching table while working with it (best practice)
            tbl.detach();

            var tbl_row_len = tbl_row.length,
                tbl_header_len = tbl_header.length,
                current_header, current_row;

            // get every row
            for (i = 0; i < tbl_row_len; i++) {
                current_row = $(tbl_row[i]);

                // get every header to make a pseudo element for it
                for (x = 0; x < tbl_header_len; x++) {
                    current_header = $(tbl_header[x]).text();
                    current_row.children('*:eq(' + [x] + ')').attr('data-header', current_header);
                }
            }

            // attaching table after working with it (best practice)
            tbl_parent.append(tbl);
        }
    },
    build: function () {
        table_responsive_stack.init();
    }

}
$(function () {
    table_responsive_stack.build();
});