(function(){var t,i,n,e,s,r,o,a,u,d,c,m;n=100,t=[[85,71,106],[174,61,99],[219,56,83],[244,92,68],[248,182,70]],e=2*Math.PI,s=document.getElementById("confetti"),o=s.getContext("2d"),window.w=0,window.h=0,c=function(){return window.w=s.width=window.innerWidth,window.h=s.height=window.innerHeight},window.addEventListener("resize",c,!1),window.onload=function(){return setTimeout(c,0)},d=function(t,i){return(i-t)*Math.random()+t},a=function(t,i,n,s){return o.beginPath(),o.arc(t,i,n,0,e,!1),o.fillStyle=s,o.fill()},m=.5,document.onmousemove=function(t){return m=t.pageX/w},window.requestAnimationFrame=function(){return window.requestAnimationFrame||window.webkitRequestAnimationFrame||window.mozRequestAnimationFrame||window.oRequestAnimationFrame||window.msRequestAnimationFrame||function(t){return window.setTimeout(t,1e3/60)}}(),i=function(){function i(){this.style=t[~~d(0,5)],this.rgb="rgba("+this.style[0]+","+this.style[1]+","+this.style[2],this.r=~~d(2,6),this.r2=2*this.r,this.replace()}return i.prototype.replace=function(){return this.opacity=0,this.dop=.03*d(1,4),this.x=d(-this.r2,w-this.r2),this.y=d(-20,h-this.r2),this.xmax=w-this.r,this.ymax=h-this.r,this.vx=d(0,2)+8*m-5,this.vy=.7*this.r+d(-1,1)},i.prototype.draw=function(){var t;return this.x+=this.vx,this.y+=this.vy,this.opacity+=this.dop,this.opacity>1&&(this.opacity=1,this.dop*=-1),(this.opacity<0||this.y>this.ymax)&&this.replace(),0<(t=this.x)&&t<this.xmax||(this.x=(this.x+this.xmax)%this.xmax),a(~~this.x,~~this.y,this.r,this.rgb+","+this.opacity+")")},i}(),r=function(){var t,e,s;for(s=[],u=t=1,e=n;e>=1?e>=t:t>=e;u=e>=1?++t:--t)s.push(new i);return s}(),window.step=function(){var t,i,n,e;for(requestAnimationFrame(step),o.clearRect(0,0,w,h),e=[],i=0,n=r.length;n>i;i++)t=r[i],e.push(t.draw());return e},step()}).call(this);

// Pointer-events none polyfill
function PointerEventsPolyfill(t){if(this.options={selector:"*",mouseEvents:["click","dblclick","mousedown","mouseup"],usePolyfillIf:function(){if("Microsoft Internet Explorer"==navigator.appName){var t=navigator.userAgent;if(null!=t.match(/MSIE ([0-9]{1,}[\.0-9]{0,})/)){var e=parseFloat(RegExp.$1);if(11>e)return!0}}return!1}},t){var e=this;$.each(t,function(t,n){e.options[t]=n})}this.options.usePolyfillIf()&&this.register_mouse_events()}PointerEventsPolyfill.initialize=function(t){return null==PointerEventsPolyfill.singleton&&(PointerEventsPolyfill.singleton=new PointerEventsPolyfill(t)),PointerEventsPolyfill.singleton},PointerEventsPolyfill.prototype.register_mouse_events=function(){$(document).on(this.options.mouseEvents.join(" "),this.options.selector,function(t){if("none"==$(this).css("pointer-events")){var e=$(this).css("display");$(this).css("display","none");var n=document.elementFromPoint(t.clientX,t.clientY);return e?$(this).css("display",e):$(this).css("display",""),t.target=n,$(n).trigger(t),!1}return!0})};

var PopupCountdown = {
    is_sup_t: true,
	bind: function() {
		$(document).on('click', '#btnCloseBdayGreeting', PopupCountdown.remove );
		$('#birthdaySecondCountdown').text('7');
	},
	timer: function() {
		var t, limit = 15;
		t = setInterval(function(){
			$('#birthdaySecondCountdown').text(limit--);
			if (limit == 0) { 
				clearInterval(t);
				PopupCountdown.remove();
			}
		}, 1000)
	},
	remove: function() {
		var card = $('#popupBirthday'), confetti = $('#confetti'), backdrop = $('#popupBackdrop');
		confetti.velocity({ opacity: 0 }, { easing: [.44,.75,.33,1], duration: 800, display: 'none',
			complete: function() {
				confetti.remove();
				card.velocity({ translateY: '-200%' },{ easing: [.96,.19,.08,.86], duration: 700,
					complete: function() { 
						card.remove();
						backdrop.velocity({ opacity: 0 }, { easing: [.3,.6,.49,.76], duration: 300, display: 'none',
							complete: function() { backdrop.remove(); }
						});
					}
				});
			}
		});
	},
	slide_timer: function () {
	    var st;
	    st = setTimeout(function () {
	        PopupCountdown.slide_toaster();
	        clearTimeout(st);
	    }, 3000);
	},

	hide_toaster: function () {
	    $(document).on('click','#closeSlider', function () { PopupCountdown.slide_toaster(); })
	},

	slide_toaster: function () {
	    $('#popupBirthdaySup').velocity({ translateX: toggle_slide() }, {
	        duration: 800,
	        easing: [0.215, .61, .355, 1],
	        complete: function () {
	            PopupCountdown.is_sup_t == true ? PopupCountdown.is_sup_t = false : PopupCountdown.is_sup_t = true;
	        }
	    });

	    function toggle_slide() {
	        if (PopupCountdown.is_sup_t == true) { return '-600px'; }
	        else { return '600px'; }
	    }

	},


	build: function () {
	    this.timer();
	    this.bind();
	    this.slide_timer();
	    this.hide_toaster();
	}
}

$(document).ready(function () {
    PopupCountdown.build();
    PointerEventsPolyfill.initialize({});
});


